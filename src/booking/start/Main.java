package booking.start;

import booking.City;
import booking.Country;
import booking.Hotels;
import booking.Room;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Country country = new Country("Ukraine", 45_000_000);

        City city1 = new City("Cherkassy");
        City city2 = new City("Kiev");

        Hotels olimpia = new Hotels("Olimpia");
        Hotels dnipro = new Hotels("Dnipro");

        Room room = new Room(4, 4.9, 140, 1500);

        Scanner scan = new Scanner(System.in);
        int method;
        boolean exit;
        exit = true;
        while (exit == true) {
            System.out.println("Меню:");
            System.out.println("1 - Информация о стране");
            System.out.println("2 - Информация о городах");
            System.out.println("3 - Информация о отелях ");
            System.out.println("4 - Информация о комнате  ");
            System.out.println("5 - Оренда комнаты ");

            System.out.print("Ввыбеете значение для вызова метода: ");
            method = scan.nextInt();
            switch (method) {
                case 1: {
                    country.infoCountry();
                    break;

                }
                case 2: {
                    city1.infoCity();
                    city2.infoCity();

                    break;
                }
                case 3: {
                    olimpia.infoHotel();
                    dnipro.infoHotel();

                    break;
                }
                case 4: {
                    room.infoRoom();

                    break;

                }
                case 5: {
                    System.out.println("Введите дату начала");
                    String startDate= scan.next();
                    System.out.println("Введите дату конца");
                    String endDate= scan.next();
                    room.validation(startDate,endDate);
                    exit = false;
                    break;

                }
                case 6:
                default:
                    System.out.println("ИДИ Н*Х*Й С ТАКИМИ ЗАПРОСАМИ");
                    break;

            }
        }
    }
}


