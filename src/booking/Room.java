package booking;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Room {

    private int numberOfRooms;
    private double roomRating;
    private int number;
    private double price;
    private double sum;


    public Room(int numberOfRooms, double roomRating, int number, double price) {
        this.numberOfRooms = numberOfRooms;
        this.roomRating = roomRating;
        this.number = number;
        this.price = price;
    }

    public void validation(String startDate, String endDate) {
        LocalDate date1 = Utils.dataFormat(startDate);
        LocalDate date2 = Utils.dataFormat(endDate);
        Long sumDate = ChronoUnit.DAYS.between(date1,date2);

        if (date2.isAfter(date1) && date1.isBefore(date2)) {
            order(sumDate);
            infoRoom();

        } else if (date1.isAfter(date2) && date2.isBefore(date1)) {
            System.err.println("Вы ввели неверную дату. Введите корректную дату : Дата начала " + endDate + " Дата конца " + startDate);
        }

    }
    public void order(Long sumDate) {
        sum = sumDate * price;
        System.out.println("Общая сумма заказа " + sum + "  Колличество дней " + sumDate);

    }

    public void infoRoom() {
        System.out.println("Названия номера " + number + " Рейтинг номера " + roomRating + " Колличество комнат " + numberOfRooms + " Цена за 1 ночь " + price + " Грн ");
    }
}