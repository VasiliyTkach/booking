package booking;

public class Hotels {
    private String nameHotel;

    public Hotels(String nameHotel) {
        this.nameHotel = nameHotel;
    }

    public void infoHotel() {
        System.out.println("Названия отеля " + nameHotel);
    }
}
